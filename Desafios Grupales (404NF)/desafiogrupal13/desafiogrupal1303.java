package desafiogrupal1303;

import java.util.Scanner;

class Main {

    public static void main(String[] args) {

        int cont;
        Datos datos = new Datos();
        Datos.Imprimir_datos(datos);
    }
}
class Datos{

    private String nombre[] = new String[3];
    private int documento[] = new int[3];
    private int edad[] = new int[3];

    public Datos(){

        int cont;
        Scanner scan = new Scanner(System.in);

        for(cont=0; cont<3; cont++) {

            System.out.println("Persona " + (cont+1));
            System.out.println("Ingrese su nombre");
            this.nombre[cont] = scan.next();

            System.out.println("Ingrese su Dni");
            do {
                this.documento[cont] = scan.nextInt();
            } while (this.documento[cont] > 80000000 || this.documento[cont] < 10000000);

            System.out.println("Ingrese su edad");
            do {
                this.edad[cont] = scan.nextInt();
            } while (this.edad[cont] > 120 || this.edad[cont] < 0);

        }

    }

    public static void Imprimir_datos(Datos x){

        int cont;

        for(cont=0; cont<3; cont++){
            System.out.print(x.nombre[cont] + "\t");
        }

        System.out.println("");
        for(cont=0; cont<3; cont++){
            System.out.print(x.documento[cont] + "\t");
        }

        System.out.println("");
        for(cont=0; cont<3; cont++){
            System.out.print(x.edad[cont] + "\t\t\t");
        }

    }

}
