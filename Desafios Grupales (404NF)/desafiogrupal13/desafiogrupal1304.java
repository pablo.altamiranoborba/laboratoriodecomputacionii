package desafiogrupal1304;

class Main {

    public static void main(String[] args) {

        float matriz[][] = new float[3][3];
        for(int a=0; a<3; a++) {
            for(int b=0; b<3; b++) {
                matriz[a][b] = (float) Math.random()*10;
            }
        }
        mostrar_matriz(matriz);
        ordenar(matriz);
    }

    public static void mostrar_matriz(float matriz[][]) {

        for(int a=0; a<3; a++) {
            for(int b=0; b<3; b++) {
                System.out.print("[" + matriz[a][b] + "]" + " ");
            }
            System.out.println();
        }
        System.out.println();

    }

    public static void ordenar(float matriz[][]) {

        float temp = 0;
        int a, b, j, i = 0;
        for(a=0; a<3; a++) {
            for(b=0; b<3; b++) {
                for(j=0; j<3; j++) {
                    for(i=0; i<3; i++) {
                        if(matriz[a][b] < matriz[j][i]) {
                            temp = matriz[j][i];
                            matriz[j][i] = matriz[a][b];
                            matriz[a][b] = temp;
                        }
                    }
                }
            }
        }
        mostrar_matriz(matriz);

    }

}
