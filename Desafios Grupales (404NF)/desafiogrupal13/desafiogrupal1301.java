package desafiogrupal1301;

class Main{

    public static void main(String[] args) {

        int array[] = new int[100];
        int cont, cont2;   //contador
        boolean repetido;
        for(cont=0; cont<100; cont++)
        {
            do {
                array[cont] = (int) (Math.random()*100) + 1; //Math.random es lo que devuelve el valor aleatorio
                repetido = false;
                for(cont2=0; cont2<cont; cont2++){
                    if(array[cont2] == array[cont]) repetido = true;
                }
            } while (repetido);
        }      
        for (int elemento : array) {
            System.out.print(elemento + " ");
        }

    }

}
