package desafiogrupal1302;

class Main {

    public static void main(String[] args) {

        float matriz[][] = new float[3][3];
        for(int a=0; a<3; a++) {
            for(int b=0; b<3; b++) {
                matriz[a][b] = (float) Math.random()*10;
            }
        }
        mostrar_matriz(matriz);

    }

    public static void mostrar_matriz(float matriz[][]) {

        for(int a=0; a<3; a++) {
            for(int b=0; b<3; b++) {
                System.out.print("[" + matriz[a][b] + "]" + " ");
            }
            System.out.println();
        }
        System.out.println();

    }

}

