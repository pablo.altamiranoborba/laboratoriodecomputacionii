package desafiogrupal1305;

import java.util.Scanner;

class Main {

    public static void main(String[] args) {

        Scanner scan = new Scanner(System.in);
        float matriz[][] = new float[3][3];
        for(int x=0; x<3; x++) {
            for(int b=0; b<3; b++) {
                System.out.println("Ingresar valor flotante elemento " + x + " " + b);
                matriz[x][b] = scan.nextFloat();
            }
        }
        mostrar_matriz(matriz);
        ordenar(matriz);

    }

    public static void mostrar_matriz(float matriz[][]) {

        for(int x=0; x<3; x++) {
            for(int b=0; b<3; b++) {
                System.out.print("[" + matriz[x][b] + "]" + " ");
            }
            System.out.println();
        }
        System.out.println();

    }

    public static void ordenar(float matriz[][]) {

        float temp = 0;
        int x, b, j, i = 0;
        for(x=0; x<3; x++) {
            for(b=0; b<3; b++) {
                for(j=0; j<3; j++) {
                    for(i=0; i<3; i++) {
                        if(matriz[x][b] < matriz[j][i]) {
                            temp = matriz[j][i];
                            matriz[j][i] = matriz[x][b];
                            matriz[x][b] = temp;
                        }
                    }
                }
            }
        }
        mostrar_matriz(matriz);
    	}
	}