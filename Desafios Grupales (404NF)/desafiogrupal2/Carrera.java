package desafiogrupal2;

import java.util.Arrays;
import java.util.Scanner;

public class Carrera implements Informacion {

    private String nombre;
    private Materia coleccionMaterias[] = new Materia[50];
    Scanner scan = new Scanner(System.in);

    Carrera(String nombre, Materia coleccionMaterias[])
    {
        this.nombre = nombre;
        this.coleccionMaterias = coleccionMaterias;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Materia[] getColeccionMaterias() {
        return this.coleccionMaterias;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setColeccionMaterias(Materia[] coleccionMaterias) { this.coleccionMaterias = coleccionMaterias; }

    public void agregarMateria(Materia materia)
    {
        this.coleccionMaterias[this.coleccionMaterias.length] = materia;
    }

    public void eliminarMateria(String nombreMateria)
    {
        int i;

        for (i = 0; i < this.coleccionMaterias.length; i++)
        {
            if (this.coleccionMaterias[i].getNombre().equals(nombreMateria)) break;
        }

        for (; i < this.coleccionMaterias.length-1; i++)
        {
            this.coleccionMaterias[i] = this.coleccionMaterias[i+1];
        }

        if (i < this.coleccionMaterias.length) this.coleccionMaterias[i] = null;
    }

    public void encontrarMateria(String nombre)
    {
        int i;

        for (i = 0; i < this.coleccionMaterias.length; i++)
        {
            if (this.coleccionMaterias[i].getNombre().equals(nombre)) break;
        }

        System.out.println("�Desea eliminar la materia?(s/n)");
        char Si_No;
        do {
            Si_No = scan.next().charAt(0);
        } while (Si_No != 's' && Si_No != 'n');

        if(Si_No == 's') this.eliminarMateria(nombre);
    }

    @Override
    public String toString() {
        return "Carrera{" +
                "nombre='" + nombre + '\'' +
                ", coleccionMaterias=" + Arrays.toString(coleccionMaterias) +
                '}';
    }

    @Override
    public int verCantidad() {
        return this.coleccionMaterias.length;
    }

    @Override
    public String listarContenidos() {
        String nombreMateria[] = new String[50];
        String nombresMaterias = "";

        for (int i = 0; i < this.coleccionMaterias.length; i++)
        {
            nombreMateria[i] = this.coleccionMaterias[i].getNombre();
        }
        Arrays.sort(nombreMateria);

        for (int i = 0; i < this.coleccionMaterias.length; i++)
        {
            nombresMaterias = nombresMaterias + "\n" + nombreMateria[i];
        }

        return nombresMaterias;
    }
}
