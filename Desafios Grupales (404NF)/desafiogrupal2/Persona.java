package desafiogrupal2;

import java.util.Scanner;

public class Persona {

    private String nombre;
    private String apellido;
    private int legajo;
    Scanner scan = new Scanner(System.in);

    Persona(String nombre, String apellido, int legajo)
    {
        this.nombre = nombre;
        this.apellido = apellido;
        this.legajo = legajo;
    }

    public String getNombre() {
        return this.nombre;
    }

    public String getApellido() {
        return this.apellido;
    }

    public int getLegajo() {
        return this.legajo;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    public void setLegajo(int legajo) {
        this.legajo = legajo;
    }

    public void modificarDatos()
    {
        do {
        System.out.println("Ingrese n�mero de legajo");
        String x = scan.next();
        this.legajo = ComprobarDato.try_catch(x);
        } while (legajo < 1);

        System.out.println("Ingrese nombre");
        this.nombre = scan.next();

        System.out.println("Ingrese apellido");
        this.apellido = scan.next();

    }

    @Override
    public String toString() {
        return "nombre='" + nombre + '\'' +
                ", apellido='" + apellido + '\'' +
                ", legajo=" + legajo +
                '}';
    }
}
