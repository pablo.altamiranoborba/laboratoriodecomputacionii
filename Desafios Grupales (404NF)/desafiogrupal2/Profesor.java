package desafiogrupal2;

public class Profesor extends Persona {
    private double basico;
    private int antiguedad;

    Profesor(double basico, int antiguedad, String nombre, String apellido, int legajo)
    {
        super(nombre, apellido, legajo);
        this.basico = basico;
        this.antiguedad = antiguedad;
    }

    public double getBasico() {
        return this.basico;
    }

    public int getAntiguedad() {
        return this.antiguedad;
    }

    public void setBasico(double basico) {
        this.basico = basico;
    }

    public void setAntiguedad(int antiguedad) {
        this.antiguedad = antiguedad;
    }

    public double calcularSueldo()
    {
        return this.basico + (0.1 * this.antiguedad);
    }

    @Override
    public String toString() {
        return "Profesor{" + super.toString() +
                "basico=" + basico +
                ", antiguedad=" + antiguedad +
                '}';
    }
}
