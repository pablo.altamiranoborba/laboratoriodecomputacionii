package desafiogrupal2;

import java.util.Arrays;
import java.util.Scanner;

public class Facultad implements Informacion {

    private String nombre;
    private Carrera coleccionCarreras[] = new Carrera[20];
    Scanner scan = new Scanner(System.in);

    Facultad(String nombre, Carrera coleccionCarreras[])
    {
        this.nombre = nombre;
        this.coleccionCarreras = coleccionCarreras;
    }

    public Carrera[] getColeccionCarreras() {
        return this.coleccionCarreras;
    }

    public String getNombre() {
        return this.nombre;
    }

    public void setColeccionCarreras(Carrera[] coleccionCarreras) {
        this.coleccionCarreras = coleccionCarreras;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void agregarCarrera(Carrera carrera)
    {
        this.coleccionCarreras[this.coleccionCarreras.length] = carrera;
    }

    public void eliminarCarrera(String nombre)
    {
        int i;

        for (i = 0; i < this.coleccionCarreras.length; i++)
        {
            if (this.coleccionCarreras[i].getNombre().equals(nombre)) break;
        }

        for(; i < this.coleccionCarreras.length-1; i++)
        {
            this.coleccionCarreras[i] = this.coleccionCarreras[i+1];
        }

        if(i < this.coleccionCarreras.length) this.coleccionCarreras[i] = null;
    }

    public void eliminarEstudiante(Estudiante estudiante)
    {
        for (int x = 0; x < this.coleccionCarreras.length; x++)
        {
            for (int y = 0; y < this.coleccionCarreras[x].getColeccionMaterias().length; y++)
            {
                this.coleccionCarreras[x].getColeccionMaterias()[y].eliminarEstudiante(estudiante.getLegajo());
            }
        }
    }

    @Override
    public String toString() {
        return "Facultad{" +
                "nombre='" + nombre + '\'' +
                ", coleccionCarreras=" + Arrays.toString(coleccionCarreras) +
                '}';
    }

    @Override
    public int verCantidad() {
        return this.coleccionCarreras.length;
    }

    @Override
    public String listarContenidos() {
        String nombreCarrera[] = new String[coleccionCarreras.length];
        String nombresCarreras = "";

        for (int i = 0; i < this.coleccionCarreras.length; i++)
        {
        	nombreCarrera[i] = this.coleccionCarreras[i].getNombre();
        }
        Arrays.sort(nombreCarrera);

        for (int i = 0; i < this.coleccionCarreras.length; i++)
        {
            nombresCarreras = nombresCarreras + "\n" + nombreCarrera[i];
        }

        return nombresCarreras;
    }
}
