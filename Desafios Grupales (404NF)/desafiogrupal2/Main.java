package desafiogrupal2;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {

        boolean power = true;
        Scanner sc = new Scanner(System.in);

        System.out.println("Ingrese cantidad m�xima de carreras");
        int CantidadCarreras = 0;

        do {
            String x = sc.next();
            CantidadCarreras = ComprobarDato.try_catch(x);
        } while (CantidadCarreras < 1);

        Carrera carreras[] = new Carrera[CantidadCarreras];

        System.out.println("Ingrese cantidad m�xima de estudiantes de la facultad");
        int MaxEstudiantesFacultad = 0;

        do {
            String x = sc.next();
            MaxEstudiantesFacultad = ComprobarDato.try_catch(x);
        } while (MaxEstudiantesFacultad < 1);

        Estudiante EstudiantesFacultad[] = new Estudiante[MaxEstudiantesFacultad];

        System.out.println("Ingrese cantidad m�xima de profesores");
        int MaxProfesores = 0;

        do {
            String x = sc.next();
            MaxProfesores = ComprobarDato.try_catch(x);
        } while (MaxProfesores < 1);

        Profesor profesores[] = new Profesor[MaxProfesores];

        Facultad facultad = new Facultad("Facultad Regional de Resistencia", carreras);

        while (power) {
            System.out.println("Bienvenido al administrador de la Facultad, que desea hacer?");
            System.out.println("[1] Mostrar informaci�n");
            System.out.println("[2] Editad facultad");
            System.out.println("[0] Terminar operacion");

            int desicion = 3;
            String s = sc.next();
            desicion = ComprobarDato.try_catch(s);

            switch (desicion) {
                case 1:{
                    System.out.println("-------Info-------");
                    System.out.println("[1]Info facultad");
                    System.out.println("[2]Info carrera");
                    System.out.println("[3]Info materia");

                    int opcion = 4;
                    do {

                        s = sc.next();
                        opcion = ComprobarDato.try_catch(s);

                        switch (opcion)
                        {
                            case 1:
                            {
                                System.out.println("Carreras:\n" + facultad.listarContenidos());
                                System.out.println("Cantidad de carreras = " + facultad.verCantidad());
                                break;
                            }
                            case 2:
                            {
                                System.out.println("Escriba el nombre de la carrera");
                                String NombreCarrera = sc.next();

                                int i;
                                for (i = 0; i < facultad.getColeccionCarreras().length; i++)
                                {
                                    if(facultad.getColeccionCarreras()[i].getNombre().equals(NombreCarrera)) break;
                                }

                                if (i < facultad.getColeccionCarreras().length)
                                {
                                    System.out.println("Carrera: " + NombreCarrera + "\nMaterias:\n" + facultad.getColeccionCarreras()[i].listarContenidos());
                                    System.out.println("Cantidad de materias = " + facultad.getColeccionCarreras()[i].verCantidad());
                                }
                                else System.out.println("La carrera no existe");

                                break;
                            }
                            case 3:
                            {
                                System.out.println("Escriba el nombre de la carrera");
                                String NombreCarrera = sc.next();

                                System.out.println("Escriba el nombre de la materia");
                                String NombreMateria = sc.next();

                                int i;
                                for (i = 0; i < facultad.getColeccionCarreras().length; i++)
                                {
                                    if(facultad.getColeccionCarreras()[i].getNombre().equals(NombreCarrera)) break;
                                }

                                if (i < facultad.getColeccionCarreras().length)
                                {
                                    int x;
                                    for (x = 0; x < facultad.getColeccionCarreras()[i].getColeccionMaterias().length; x++)
                                    {
                                        if(facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].getNombre().equals(NombreMateria)) break;
                                    }

                                    if (x < facultad.getColeccionCarreras()[i].getColeccionMaterias().length)
                                    {
                                        System.out.println("Carrera: " + NombreCarrera + "\nMaterias:" + NombreMateria + "\nEstudiantes:\n" + facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].listarContenidos());
                                        System.out.println("Cantidad de estudiantes = " + facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].verCantidad());
                                    }
                                    else System.out.println("La materia no existe");
                                }
                                else System.out.println("La carrera no existe");

                                break;
                            }
                        }
                    } while (opcion < 1 || opcion > 3);
                    break;
                }
                case 2:{
                    System.out.println("-------Facultad-------");
                    System.out.println("[1]Cambiar nombre");
                    System.out.println("[2]Editar carrera");
                    System.out.println("[3]Elininar carrera");
                    System.out.println("[4]Eliminar estudiantes de la facultad");

                    int opcion = 5;
                    do {

                        s = sc.next();
                        opcion = ComprobarDato.try_catch(s);

                        switch (opcion) {
                            case 1:
                            {
                                System.out.println("Ingrese nuevo nombre");
                                facultad.setNombre(sc.next());
                                break;
                            }
                            case 2:
                            {
                                System.out.println("Ingrese el nombre de la carrera que quiere modificar");
                                String NombreCarrera = sc.next();
                                int i;
                                for(i = 0; i < facultad.getColeccionCarreras().length; i++)
                                {
                                    if(facultad.getColeccionCarreras()[i].getNombre().equals(NombreCarrera)) break;
                                }

                                if(i < facultad.getColeccionCarreras().length) {
                                    System.out.println("[1]Cambiar nombre");
                                    System.out.println("[2]Editar materia");
                                    System.out.println("[3]Eliminar materia");
                                    System.out.println("[4]Buscar materia");

                                    opcion = 5;
                                    do {
                                        s = sc.next();
                                        opcion = ComprobarDato.try_catch(s);

                                        switch (opcion) {
                                            case 1:
                                            {
                                                System.out.println("Ingrese nombre de la carrera");
                                                facultad.getColeccionCarreras()[i].setNombre(sc.next());
                                                break;
                                            }
                                            case 2: case 3: case 4:
                                            {
                                                System.out.println("Ingrese nombre de la materia");
                                                String NombreMateria = sc.next();

                                                if(opcion == 2) {
                                                    int x;
                                                    for(x = 0; x < facultad.getColeccionCarreras()[i].getColeccionMaterias().length; x++)
                                                    {
                                                        if(facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].getNombre().equals(NombreMateria)) break;
                                                    }

                                                    if (x < facultad.getColeccionCarreras()[i].getColeccionMaterias().length) {
                                                        System.out.println("[1]Cambiar nombre");
                                                        System.out.println("[2]Cambiar titular");
                                                        System.out.println("[3]Editar estudiante");
                                                        System.out.println("[4]Eliminar estudiante");

                                                        opcion = 5;
                                                        do {
                                                            String y = sc.next();
                                                            opcion = ComprobarDato.try_catch(y);

                                                            switch (opcion)
                                                            {
                                                                case 1:
                                                                {
                                                                    System.out.println("Ingrese nombre de la materia");
                                                                    facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].setNombre(sc.next());
                                                                    break;
                                                                }
                                                                case 2:
                                                                {
                                                                    Profesor titular = titular(profesores, EstudiantesFacultad);

                                                                    int i1 = ComprobarProfesor(titular, profesores);
                                                                    if (i1 == -1) profesores[profesores.length] = titular;
                                                                    else titular = profesores[i1];

                                                                    facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].setTitular(titular);

                                                                    break;
                                                                }
                                                                case 3:
                                                                {
                                                                    boolean cursando_materia;

                                                                    do {
                                                                        Estudiante estudiante = null;
                                                                        cursando_materia = false;
                                                                        estudiante.modificarDatos();

                                                                        for(int i1 = 0; i1 < facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].getColeccionEstudiantes().length; i1++)
                                                                        {
                                                                            if (facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].getColeccionEstudiantes()[i1].getLegajo() == estudiante.getLegajo())
                                                                            {
                                                                                cursando_materia = true;
                                                                                break;
                                                                            }
                                                                        }

                                                                        if (!cursando_materia) {
                                                                            int i1;
                                                                            for (i1 = 0; i1 < EstudiantesFacultad.length; i1++) {
                                                                                if (EstudiantesFacultad[i1].getLegajo() == estudiante.getLegajo()) {
                                                                                    estudiante = EstudiantesFacultad[i1];
                                                                                }
                                                                            }
                                                                            if (i1 == EstudiantesFacultad.length)
                                                                            {
                                                                                EstudiantesFacultad[EstudiantesFacultad.length] = estudiante;
                                                                            }
                                                                        }

                                                                        facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].agregarEstudiante(estudiante);

                                                                    } while (cursando_materia);
                                                                    break;
                                                                }
                                                                case 4:
                                                                {
                                                                    System.out.println("Ingrese n�mero de legajo del estudiante");
                                                                    int legajo = 0;

                                                                    do {
                                                                        y = sc.next();
                                                                        legajo = ComprobarDato.try_catch(y);
                                                                    } while (legajo >= 0);

                                                                    facultad.getColeccionCarreras()[i].getColeccionMaterias()[x].eliminarEstudiante(legajo);
                                                                    break;
                                                                }
                                                            }
                                                        } while (opcion < 1 || opcion > 4);
                                                    }
                                                    else
                                                    {
                                                        char Si_No;
                                                        do {
                                                            System.out.println("La materia no existe, �Desea crearla?(s/n)");
                                                            Si_No = sc.next().charAt(0);
                                                        } while (Si_No != 's' && Si_No != 'n');
                                                        if(Si_No == 's')
                                                        {
                                                            System.out.println("Ingrese nombre de la materia");
                                                            String Nombre = sc.next();

                                                            System.out.println("Ingrese cantidad m�xima de estudiantes de la materia");
                                                            int MaxEstudiantes = 0;

                                                            do {
                                                                String z = sc.next();
                                                                MaxEstudiantes = ComprobarDato.try_catch(z);
                                                            } while (MaxEstudiantes >= 0);

                                                            Estudiante EstudiantesMateria[] = new Estudiante[MaxEstudiantes];

                                                            Profesor titular = titular(profesores, EstudiantesFacultad);

                                                            int i1 = ComprobarProfesor(titular,profesores);
                                                            if (i1 == -1) profesores[profesores.length] = titular;
                                                            else titular = profesores[i1];

                                                            if (i1 == profesores.length) profesores[profesores.length] = titular;

                                                            Materia materia = new Materia(Nombre, titular, EstudiantesMateria);

                                                            facultad.getColeccionCarreras()[i].agregarMateria(materia);
                                                        }
                                                    }
                                                }
                                                else if (opcion == 3) facultad.getColeccionCarreras()[i].eliminarMateria(NombreMateria);
                                                else facultad.getColeccionCarreras()[i].encontrarMateria(NombreMateria);

                                                opcion = 2;

                                                break;
                                            }
                                        }
                                    } while (opcion < 1 || opcion > 4);
                                }
                                else
                                {
                                    System.out.println("La carrera no existe, �Desea crearla?(s/n)");
                                    char Si_No;

                                    do {
                                        Si_No = sc.next().charAt(0);
                                    } while (Si_No != 's' && Si_No != 'n');

                                    if(Si_No == 's')
                                    {
                                        System.out.println("Ingrese cantidad m�xima de materias");

                                        int CantidadMaterias = 0;

                                        do {
                                            s = sc.next();
                                            CantidadMaterias = ComprobarDato.try_catch(s);
                                        } while (CantidadMaterias < 1);

                                        Materia materiasCarrera[] = new Materia[CantidadMaterias];

                                        System.out.println("Ingrese nombre de la carrera");
                                        Carrera carrera = new Carrera(sc.next(), materiasCarrera);

                                        int materias = 0;
                                        System.out.println("Ingrese cantidad actual de materias");

                                        do {
                                            s = sc.next();
                                            materias = ComprobarDato.try_catch(s);
                                        } while (materias < 1 || materias > CantidadMaterias);

                                        for (int j = 0; j < materias; j++)
                                        {
                                            Profesor profesor = titular(profesores, EstudiantesFacultad);

                                            int q = ComprobarProfesor(profesor, profesores);
                                            if (q == -1) profesores[profesores.length] = profesor;
                                            else profesor = profesores[q];

                                            System.out.println("Ingrese cantidad m�xima de estudiantes");
                                            int CantidadEstudiantes = 0;

                                            do {
                                                s = sc.next();
                                                CantidadEstudiantes = ComprobarDato.try_catch(s);
                                            } while (CantidadEstudiantes < 1);

                                            Estudiante estudiantes[] = new Estudiante[CantidadEstudiantes];

                                            System.out.println("Ingrese nombre de la materia");
                                            Materia materia = new Materia(sc.next(), profesor, estudiantes);

                                            System.out.println("Salario = " + profesor.calcularSueldo());

                                            System.out.println("Ingrese cantidad actual de estudiantes");
                                            int cantidad_estudiantes = 0;

                                            do {
                                                s = sc.next();
                                                cantidad_estudiantes = ComprobarDato.try_catch(s);
                                            } while (cantidad_estudiantes < 1 || cantidad_estudiantes > CantidadEstudiantes);

                                            for (int y = 0; y < cantidad_estudiantes; y++)
                                            {
                                                Estudiante estudiante = null;

                                                do {
                                                    estudiante.modificarDatos();
                                                } while (estudiante.getLegajo() == profesor.getLegajo());

                                                int z;
                                                for (z = 0; z < EstudiantesFacultad.length; z++)
                                                {
                                                    if (EstudiantesFacultad[z].getLegajo() == estudiante.getLegajo()) {
                                                        estudiante = EstudiantesFacultad[z];
                                                        break;
                                                    }
                                                }

                                                if(z == EstudiantesFacultad.length) EstudiantesFacultad[z] = estudiante;

                                                materia.agregarEstudiante(estudiante);
                                            }

                                            carrera.agregarMateria(materia);
                                        }

                                        facultad.agregarCarrera(carrera);
                                    }
                                }
                                opcion = 2;

                                break;
                            }
                            case 3:
                            {
                                System.out.println("Ingrese el nombre de la carrera que desea eliminar");
                                facultad.eliminarCarrera(sc.next());
                                break;
                            }
                            case 4:
                            {
                                System.out.println("Ingrese el n�mero de legajo del estudiante que desea eliminar");
                                int legajo = 0;

                                do {
                                    s = sc.next();
                                    legajo = ComprobarDato.try_catch(s);
                                } while (legajo < 1);

                                int i;
                                for (i = 0; i < EstudiantesFacultad.length; i++) {
                                    if(EstudiantesFacultad[i].getLegajo() == legajo) break;
                                }

                                if(i < EstudiantesFacultad.length) {
                                    Estudiante estudiante_eliminado = EstudiantesFacultad[i];
                                    facultad.eliminarEstudiante(estudiante_eliminado);

                                    for (; i < EstudiantesFacultad.length; i++) {
                                        EstudiantesFacultad[i] = EstudiantesFacultad[i+1];
                                    }
                                }
                                else System.out.println("No existe el estudiante");
                            }
                        }

                    } while (opcion < 1 || opcion > 4);

                    break;
                }

                case 0:{
                    power = false;
                }

            }

        }


    }

    public static Profesor titular(Profesor profesores[], Estudiante estudiantes[])
    {
        Profesor titular = null;
        boolean estudiante;
        boolean profesor_registrado = false;
        Scanner sc = new Scanner(System.in);

        do{
            estudiante = false;
            System.out.println("Ingrese datos del titular de la materia");
            titular.modificarDatos();

            for(int i1 = 0; i1 < profesores.length; i1++)
            {
                if(estudiantes[i1].getLegajo() == titular.getLegajo())
                {
                    System.out.println("Esta persona est� registrada como estudiante");
                    estudiante = true;
                    break;
                }
            }

            for(int i1 = 0; i1 < profesores.length; i1++)
            {
                if(estudiante) break;
                else if(profesores[i1].getLegajo() == titular.getLegajo())
                {
                    titular = profesores[i1];
                    profesor_registrado = true;
                    break;
                }
            }
        } while (estudiante);

        if(!profesor_registrado) {
            System.out.println("Ingrese a�os de antiguedad");
            int antiguedad = -1;

            do {
                String x = sc.next();
                antiguedad = ComprobarDato.try_catch(x);
            } while (antiguedad < 0);

            titular.setBasico(25000);
        }

        return titular;
    }

    public static int ComprobarProfesor (Profesor profesor, Profesor[] profesores)
    {
        int q;
        for (q = 0; q < profesores.length; q++)
        {
            if (profesor.getLegajo() == profesores[q].getLegajo()) break;
        }
        if (q == profesores.length) return -1;
        else return q;
    }

}


