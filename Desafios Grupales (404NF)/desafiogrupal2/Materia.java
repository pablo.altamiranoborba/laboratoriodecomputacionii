package desafiogrupal2;


import java.util.Arrays;
import java.util.Scanner;

public class Materia implements Informacion {

    private String nombre;
    private Profesor titular;
    private Estudiante coleccionEstudiantes[] = new Estudiante[200];
    Scanner scan = new Scanner(System.in);

    Materia(String nombre, Profesor titular, Estudiante coleccionEstudiantes[])
    {
        this.nombre = nombre;
        this.titular = titular;
        this.coleccionEstudiantes = coleccionEstudiantes;
    }

    public String getNombre() {
        return this.nombre;
    }

    public Profesor getTitular() {
        return this.titular;
    }

    public Estudiante[] getColeccionEstudiantes() {
        return this.coleccionEstudiantes;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public void setTitular(Profesor titular) {
        this.titular = titular;
    }

    public void setColeccionEstudiantes(Estudiante[] coleccionEstudiantes) {
        this.coleccionEstudiantes = coleccionEstudiantes;
    }

    public void agregarEstudiante(Estudiante estudiante)
    {
        this.coleccionEstudiantes[this.coleccionEstudiantes.length] = estudiante;
    }

    public void eliminarEstudiante(int legajo)
    {
        int i;

        for (i = 0; i < this.coleccionEstudiantes.length; i++)
        {
            if (this.coleccionEstudiantes[i].getLegajo() == legajo) break;
        }

        for(; i < this.coleccionEstudiantes.length-1; i++)
        {
            this.coleccionEstudiantes[i] = this.coleccionEstudiantes[i+1];
        }

        if (i < this.coleccionEstudiantes.length) this.coleccionEstudiantes[i] = null;
    }

    public void modificarTitular(Profesor profesor)
    {
        this.titular = profesor;
    }

    @Override
    public String toString() {
        return "Materia{" +
                "nombre='" + nombre + '\'' +
                ", titular=" + titular +
                ", coleccionEstudiantes=" + Arrays.toString(coleccionEstudiantes) +
                '}';
    }

    @Override
    public int verCantidad() {
        return this.coleccionEstudiantes.length;
    }

    @Override
    public String listarContenidos() {
        String nombreEstudiante[] = new String[50];
        String nombresEstudiantes = "";

        for (int i = 0; i < this.coleccionEstudiantes.length; i++)
        {
            nombreEstudiante[i] = this.coleccionEstudiantes[i].getNombre();
        }
        Arrays.sort(nombreEstudiante);

        for (int i = 0; i < this.coleccionEstudiantes.length; i++)
        {
            nombresEstudiantes = nombresEstudiantes + "\n" + nombreEstudiante[i];
        }

        return nombresEstudiantes;
    }
}

