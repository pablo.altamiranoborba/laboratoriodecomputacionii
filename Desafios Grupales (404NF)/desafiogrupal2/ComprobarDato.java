package desafiogrupal2;

import java.util.Scanner;

public class ComprobarDato {
    public static int try_catch(String antiguedad) {
        Scanner sc = new Scanner(System.in);
        int x;
        try {
            x = Integer.parseInt(antiguedad);
        } catch (Exception e) {
            System.out.println("Tipo de dato erroneo");
            return -1;
        }
        return x;
    }
}
