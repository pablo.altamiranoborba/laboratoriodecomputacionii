package tpcalculadora;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class Main {
	
	public static void main(String[] args) {
		framecalculator FC = new framecalculator(); //FC nombre de la clase
		FC.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}

}
	class framecalculator extends JFrame {
	public framecalculator () {
	setTitle ("Scientific calculator");
	setBounds (472, 286, 360, 360); //especificar posicion
	setVisible (true);
	add (new panelprincipal());
	
	}
	
	}
	
	class panelprincipal extends JPanel {
	private JButton display; //creacin de botones
	private JPanel numbers;
	private boolean start = true;
	private String operation;
	private double total; 
	
	public panelprincipal () {
		
	setLayout (new BorderLayout());
	display = new JButton("0");
	display.setEnabled(false);
	add(display, BorderLayout.NORTH); //Posicin de la calculadora
	
	// Botones de la calculadora 
		
	numbers = new JPanel(); //crear un panel
	numbers.setLayout(new GridLayout(4, 4)); // tamaño de la red
	
	clearScreen clearDisplay = new clearScreen(); //Activa la funcion para limpiar la pantalla
    numDisplay numDisplay = new numDisplay(); //Activa la funcion para insertar numeros
    Operacion NumOperation = new Operacion(); //Activa la funcion para realizar operaciones
    
    newBtn("7", numDisplay);
    newBtn("8", numDisplay);
    newBtn("9", numDisplay);
    newBtn("x", NumOperation);

    newBtn("4", numDisplay);
    newBtn("5", numDisplay);
    newBtn("6", numDisplay);
    newBtn("-", NumOperation);

    newBtn("1", numDisplay);
    newBtn("2", numDisplay);
    newBtn("3", numDisplay);
    newBtn("+", NumOperation);

    newBtn("/", NumOperation);
    newBtn("0", numDisplay);
    newBtn("C", clearDisplay);
    newBtn("=", NumOperation);
    add(numbers, BorderLayout.CENTER); //Posicion central
    operation="=";
	}
	//
	
	private class numDisplay implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String numero = e.getActionCommand();

            if(start){
                display.setText("");
                start=false;

            }
            display.setText(display.getText()+numero);
        }
    }
	private class Operacion implements ActionListener{

        @Override
        public void actionPerformed(ActionEvent e) {
            String user = e.getActionCommand();
            calculo(Double.parseDouble(display.getText()));
            operation=user;
            start=true;

        }

        public void calculo(Double numero){
            try{
                if (operation.equals("+")){
                    total += numero;
                } else if (operation.equals("-")){
                     total -= numero;
                } else if (operation.equals("x")){
                     total *= numero;
                } else if (operation.equals("/")){
                     total /= numero;
                } else if (operation.equals("=")){
                     total = numero;
                }
                display.setText("" +  total);
            } catch (ArithmeticException e){
                System.out.println("No se puede dividir por 0");
            }

        }
    }
	private class clearScreen implements ActionListener{
        @Override
        public void actionPerformed(ActionEvent e) {
            display.setText("0");
        }

    }
	private void newBtn (String name, ActionListener numDisplay){

        JButton boton = new JButton(name);
        boton.addActionListener(numDisplay);
        numbers.add(boton);
    }
	}
