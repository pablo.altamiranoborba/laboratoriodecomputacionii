package desafio10;

import java.util.Scanner;

public class Main
{
    public static void main(String[] args)
    {
        int Aleatorio, Num, Intentos=0;
        boolean acertado;
        Scanner scan = new Scanner(System.in);
        Aleatorio = (int) (Math.random() * 100) + 1;
        do
        {
            Intentos++;
            do
            {
                if (Intentos == 1)
                {
                    System.out.println("Ingrese un n�mero entero entre 1 y 100");
                }
                else
                {
                    System.out.println("Prueba otra vez");
                }
                Num = scan.nextInt();
            } while (Num < 1 || Num > 100);
            if (Num < Aleatorio) {
                System.out.println("El n�mero ingresado es menor");
                acertado = false;
            } else if (Num > Aleatorio) {
                System.out.println("El n�mero ingresado es mayor");
                acertado = false;
            } else {
                System.out.println("Correcto\nN�mero de intentos: " + Intentos);
                acertado = true;
            }
        } while (!acertado);
    }
}
