package Test;

import java.io.Serializable;

public class CuentaCorriente implements Serializable {
    private String nombretitular;
    private double saldo;
    private int nrocuenta;

    public CuentaCorriente(String nombretitular, double SaldoInicial, int nrocuenta)
    {
        this.nombretitular = nombretitular;
        this.saldo = SaldoInicial;
        this.nrocuenta = nrocuenta;
    }

    public void setSaldo(Double saldo) {
        if (this.saldo + saldo < 0) System.out.println("Saldo insuficiente");
        else this.saldo += saldo;
    }

    public void igresarDinero(double dinero) {
        this.saldo = this.saldo + dinero;
    }


    public double verSaldo() {
        return saldo;
    }

    public int getNroDeCuenta() {
        return nrocuenta;
    }

    public void setNroCuenta(int nroCuenta) {
        this.nrocuenta = nroCuenta;
    }



    public String toString() {
        return "CuentaCorriente{" +
                "nombretitular='" + nombretitular + '\'' +
                ", saldo=" + saldo +
                ", nrocuenta=" + nrocuenta+
                '}';
    }

    public void TransferirDinero(CuentaCorriente Cuenta2, double saldo)
    {
        if (saldo > 0)
        {
            double ComprobarTransaccion = this.saldo;
            this.setSaldo(-saldo);
            if (this.saldo < ComprobarTransaccion) Cuenta2.setSaldo(saldo);
        }
        else System.out.println("Cantidad no valida");
    }
}
