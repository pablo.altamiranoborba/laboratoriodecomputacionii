import javax.swing.*;
import javax.swing.text.StyledEditorKit;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


class Main {

    public static void main(String[] args) {
        // TODO Auto-generated method stub

        FramePrincipal framework=new FramePrincipal();

        framework.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

    }

}

class FramePrincipal extends JFrame {

    private PanelPrincipal sheet;

    public FramePrincipal(){

        setBounds(500,300,550,420);

        sheet = new PanelPrincipal();

        setTitle("Procesador de Texto");

        add(sheet);

       addWindowListener(new EventosDeVentana());

        setVisible(true);

    }

    private class EventosDeVentana extends WindowAdapter {


        @Override
        public void windowClosing(WindowEvent e) {

            int reply = JOptionPane.showConfirmDialog(null, "Desea Guardar?", "Esta Saliendo sin guardar", JOptionPane.YES_NO_OPTION);
            if (reply == JOptionPane.YES_OPTION) {

                JFileChooser fc = new JFileChooser();
                int i = fc.showSaveDialog(sheet.areaDeTexto);

                if (i == JFileChooser.APPROVE_OPTION) {

                    File file = fc.getSelectedFile();

                    try (FileWriter fw = new FileWriter(file)) {

                        fw.write(sheet.areaDeTexto.getText());

                    } catch (FileNotFoundException el) {
                        el.printStackTrace();
                    } catch (IOException el) {
                        el.printStackTrace();
                    }
                } else System.exit(0);
            }
        }
    }
}

class PanelPrincipal extends JPanel implements ActionListener{

    public JTextPane areaDeTexto;

    public boolean guardado=false;

    JMenu archivo;
    JMenu fuente;
    JMenu estilo;
    JMenu tamanio;
    JMenuItem Abrir;
    JMenuItem Guardar;

    public PanelPrincipal(){

        setLayout(new BorderLayout());

        JPanel sheetmenu=new JPanel();

        JMenuBar bar=new JMenuBar();

        //          Barra de menu principal


        archivo=new JMenu("Archivo");

        fuente=new JMenu("Fuente");

        estilo=new JMenu("Estilo");

        tamanio =new JMenu("tamanio");


        //----------------------------------------------------

        Abrir = new JMenuItem("Abrir");
        Guardar = new JMenuItem("Guardar");

        Abrir.addActionListener(this);
        Guardar.addActionListener( this);


        archivo.add(Abrir);
        archivo.add(Guardar);

        //----------------------------------------------------

        //configuracion fuentes de letra


        configuracion_menu("Arial","fuente","Arial",9,10);

        configuracion_menu("Courier","fuente","Courier",9,10);

        configuracion_menu("Verdana","fuente","Verdana",9,10);

        configuracion_menu("Times new Roman","fuente","Times New Roman",9,10);

        configuracion_menu("Comic Sans MS","fuente","Comic Sans MS",9,10);


        // Configuracion estilo de letra

        configuracion_menu("Negrita","estilo","",Font.BOLD,1);

        configuracion_menu("Cursiva","estilo","",Font.ITALIC,1);

        configuracion_menu("Subrayado","estilo","",Font.HANGING_BASELINE,1);

        //Agregamos los botones con los distintos tamanios de letra


        ButtonGroup tamanio_letra=new ButtonGroup();

        JRadioButtonMenuItem doce=new JRadioButtonMenuItem("12");

        JRadioButtonMenuItem catorce=new JRadioButtonMenuItem("14");

        JRadioButtonMenuItem dieciseis=new JRadioButtonMenuItem("16");

        JRadioButtonMenuItem dieciocho=new JRadioButtonMenuItem("18");

        JRadioButtonMenuItem veinte=new JRadioButtonMenuItem("20");

        JRadioButtonMenuItem veintidos=new JRadioButtonMenuItem("22");

        JRadioButtonMenuItem veinticuatro=new JRadioButtonMenuItem("24");


        tamanio_letra.add(doce);

        tamanio_letra.add(catorce);

        tamanio_letra.add(dieciseis);

        tamanio_letra.add(dieciocho);

        tamanio_letra.add(veinte);

        tamanio_letra.add(veintidos);

        tamanio_letra.add(veinticuatro);

        //Si no usamos el AListener el cambio de tamanio no tiene efecto

        doce.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", 12));

        catorce.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", 14));

        dieciseis.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", 16));

        dieciocho.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", 18));

        veinte.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", 20));

        veintidos.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", 22));

        veinticuatro.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", 24));

        tamanio.add(doce);

        tamanio.add(catorce);

        tamanio.add(dieciseis);

        tamanio.add(dieciocho);

        tamanio.add(veinte);

        tamanio.add(veintidos);

        tamanio.add(veinticuatro);


        //Se agrega la microventana o barras con sus respectivos botones

        bar.add(archivo);

        bar.add(fuente);

        bar.add(estilo);

        bar.add(tamanio);

        //			Elegimos el lugar donde se encontrara la barra con los botones del menu.

        sheetmenu.add(bar);

        add(sheetmenu,BorderLayout.NORTH);

        areaDeTexto =new JTextPane();

        add(areaDeTexto,BorderLayout.CENTER);

        JPopupMenu menuEmergente=new JPopupMenu();

        // 		Formato de la letra

        JMenuItem negritaE=new JMenuItem("Negrita");

        JMenuItem cursivaE=new JMenuItem("Cursiva");

        negritaE.addActionListener(new StyledEditorKit.BoldAction());

        cursivaE.addActionListener(new StyledEditorKit.ItalicAction());

        menuEmergente.add(negritaE);

        menuEmergente.add(cursivaE);

        areaDeTexto.setComponentPopupMenu(menuEmergente);

    }

    public void configuracion_menu(String textoMenu, String menu, String tipo_letra, int estilos, int tamanioLetra){

        JMenuItem elem_menu=new JMenuItem(textoMenu);

        // AListener Fuente de la letra
        if(menu=="fuente"){

            fuente.add(elem_menu);

            if(tipo_letra=="Arial"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Arial"));

            }else if(tipo_letra=="Courier"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Courier"));

            }else if(tipo_letra=="Verdana"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Verdana"));

            }else if (tipo_letra=="Times New Roman"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Times New Roman"));

            }else if (tipo_letra=="Comic Sans MS"){
                elem_menu.addActionListener(new StyledEditorKit.FontFamilyAction("cambia_letra","Comic Sans MS"));
            }

            //Estilo de la letra


        }else if(menu=="estilo"){

            estilo.add(elem_menu);

            if(estilos==Font.BOLD){

                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N, InputEvent.CTRL_DOWN_MASK));

                elem_menu.addActionListener(new StyledEditorKit.BoldAction());}

            else if(estilos==Font.ITALIC){

                elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_K,InputEvent.CTRL_DOWN_MASK));

                elem_menu.addActionListener(new StyledEditorKit.ItalicAction());

                if(estilos==Font.HANGING_BASELINE){
                    elem_menu.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_U,InputEvent.CTRL_DOWN_MASK));

                    elem_menu.addActionListener(new StyledEditorKit.UnderlineAction());
                }


            }else if(menu=="tamanio"){

                tamanio.add(elem_menu);

                elem_menu.addActionListener(new StyledEditorKit.FontSizeAction("cambia_tamanio", tamanioLetra));
            }

        }

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == Abrir) {
            JFileChooser fc = new JFileChooser();
            int i = fc.showOpenDialog(this);

            if (i == JFileChooser.APPROVE_OPTION) {

                File file = fc.getSelectedFile();

                if (file.exists())
                    System.out.println("El file " + file + " existe");
                else {
                    JOptionPane.showMessageDialog(this, "ERROR, el archivo que buscas no esta disponible");
                }

                try (FileReader fr = new FileReader(file)) {
                    String cadena = "";
                    int valor = fr.read();
                    while (valor != -1) {
                        cadena += (char) valor;
                        valor = fr.read();
                    }
                    this.areaDeTexto.setText(cadena);
                } catch (IOException el) {
                    el.printStackTrace();
                }
            }

        } else if (e.getSource() == Guardar) {
            JFileChooser fc = new JFileChooser();
            int i = fc.showSaveDialog(this);

            if (i == JFileChooser.APPROVE_OPTION) {

                File file = fc.getSelectedFile();

                try (FileWriter fw = new FileWriter(file)) {

                    fw.write(this.areaDeTexto.getText());

                } catch (FileNotFoundException el) {
                    el.printStackTrace();
                } catch (IOException el) {
                    el.printStackTrace();
                }
                guardado = false;
            }
        }

    }

}